DIR := src
DIRS += $(DIR)

MISC_SRCS := $(shell find $(DIR)/misc -name \*.cc)

LIB_SRCS := $(MISC_SRCS)

$(DIR)/libbiotools$(A): $(LIB_SRCS:.cc=$(O))

SRCS += $(LIB_SRCS)
BINS +=
LIBS += $(DIR)/libbiotools$(A)
