#!/bin/bash

# HOTSPOT
# 
#

tmpdir=`eval echo $1` #/tmp
outdir=$2 #/tmp

random_seed=$3 #1

bam_file=$4 #/home/jvierstra/proj/ss.dnase/data/K562-P5-20140207/align/reads.bam

contig=$5 #"chr21"
contig_sizes=$6 #/home/jvierstra/data/genomes/hg19/hg.all.chrom.sizes
ref_uniq_mappable=$7 #/data/vol7/annotations/data/hg19/hg19.K36.mappable_only.bed

background_window_size=50000

z_thresh=2
min_width=10
merge_distance=150

bindir=/home/jvierstra/proj/code/bio-tools/apps/hotspot

mkdir -p ${tmpdir}

# Makes a library of 5' tags from a BAM alignment file
# and also creates a random library

function initialize {

	python ${bindir}/generate_tag_libraries.py\
		--contig ${contig} --mappable_genome ${ref_uniq_mappable}\
		--alignment_file $1\
		--observed_tag_file $2 --random_tag_file $3

}

# Takes the tags file and finds hotspots
#
# Inputs: (1) tags 
# Outputs: (2) pass 1 hotspots

function pass1 {
	
	# find hotspots

	python ${bindir}/find_hotspots.py\
		--contig ${contig} --contig_sizes ${contig_sizes}\
		--mappable_genome ${ref_uniq_mappable}\
		--tag_file $1\
		--density_window ${background_window_size}\
	> $2

}

# Filters the pass 1 hotspots for width and z-score
# The filtered hotspots are merged.
# 
# Pass 2 involves extracting the tags from a large window
# surrounding the merged pass 1 hotspots, removing reads that
# are within pass 1 hotspots, and them filtering them for 36-bp
# mappability.
#
# Inputs: (1) tags (2) pass 1 hotspots
# Outputs: (3) pass 2 hotspots

function pass2 {
	
	# threshold

	cat $2 | awk -v OFS="\t" -v min_width=${min_width} -v z_thresh=${z_thresh}\
		'$3-$2 < min_width { next; } $5 < z_thresh { next; } { print; }'\
		| sort-bed -\
	> ${tmpdir}/thresholded.bed

	# merge within +/-75bp

	bedops --range $(echo ${merge_distance}/2 | bc) -u ${tmpdir}/thresholded.bed | bedops -m -\
		| bedops --range -$(echo ${merge_distance}/2 | bc) -u - | sort-bed -\
	> ${tmpdir}/merged.bed	

	# fetch tags

	bedops --range ${background_window_size} -m ${tmpdir}/merged.bed\
		| bedops -d - ${tmpdir}/merged.bed\
		| bedops -i - ${ref_uniq_mappable}\
		| bedops -e $1 -\
	> ${tmpdir}/filtered.tags.bed

	# hotspot detection; fuzzy z-score threshold to allow for some "squeakers"

	python ${bindir}/find_hotspots.py\
		--contig ${contig} --contig_sizes ${contig_sizes}\
		--mappable_genome ${ref_uniq_mappable}\
		--tag_file ${tmpdir}/filtered.tags.bed\
		--density_window ${background_window_size}\
		--window_thresh_fuzzy\
	> $3

	# cleanup

	rm -f ${tmpdir}/thresholded.bed ${tmpdir}/merged.bed ${tmpdir}/filtered.tags.bed

}

#output: contig start end id z-score p-value

function combine_and_rescore {

	# combine and filter pass 1 and 2

	cat $2 $3 | awk -v OFS="\t" -v min_width=${min_width} -v z_thresh=${z_thresh}\
		'$3-$2 < min_width { next; } $5 < z_thresh { next; }\
		{ pad = int($8 / 2);  print $1, $7-pad, $7+pad, "id-"NR, $2, $3, $9; }'\
		| sort-bed - > ${tmpdir}/combined.bed

	# we recompute the z-score from the original set of tags (to be consistent)
	
	# get tags and mappable sites within the background window

	cat ${tmpdir}/combined.bed | bedops --range $(echo ${background_window_size}/2 | bc) -u - | bedmap --delim "\t" --echo --count - $1\
		| bedmap --delim "\t" --echo --bases - ${contig_uniq_mappable} | cut -f8- > ${tmpdir}/combined.background.bed
	
	# compute stats (z-score, p-value) again...

	bedmap --delim "\t" --echo --bases ${tmpdir}/combined.bed ${contig_uniq_mappable} | paste - ${tmpdir}/combined.background.bed\
		| python ${bindir}/compute_stats.py\
			--contig ${contig} --mappable_genome ${ref_uniq_mappable}\
			--tag_file $1\
		| awk -v OFS="\t" '{ print $1, $5, $6, $4, $11, $12; }' | sort-bed - > $4

	rm -f ${tmpdir}/combined.bed ${tmpdir}/combined.background.bed

}

function compute_spot {

	tags_in_hotspot=$(bedops -e -1 $1 $2 | wc -l)
	tags_total=$(cat $1 | wc -l)
	spot=$(echo "scale=4; ${tags_in_hotspot}/${tags_total}" | bc)

	echo -e "${contig}\t${tags_total}\t${tags_in_hotspot}\t${spot}" > $3
}

# step 1: make libraries and random libraries

echo "${contig}: initialize"

initialize ${bam_file} ${tmpdir}/observed.bed ${tmpdir}/random.bed

# step 2: first pass hotspot on observed and random tags

echo "${contig}: pass 1"

pass1 ${tmpdir}/observed.bed ${tmpdir}/observed.hotspots.pass1.bed
pass1 ${tmpdir}/random.bed ${tmpdir}/random.hotspots.pass1.bed

# step 3: second pass on flanking regions

echo "${contig}: pass 2"

pass2 ${tmpdir}/observed.bed ${tmpdir}/observed.hotspots.pass1.bed ${tmpdir}/observed.hotspots.pass2.bed
pass2 ${tmpdir}/random.bed ${tmpdir}/random.hotspots.pass1.bed ${tmpdir}/random.hotspots.pass2.bed

# step 4: rescore

echo "${contig}: rescore"

combine_and_rescore ${tmpdir}/observed.bed ${tmpdir}/observed.hotspots.pass1.bed ${tmpdir}/observed.hotspots.pass2.bed ${outdir}/observed.${contig}.hotspots.both-passes.bed
combine_and_rescore ${tmpdir}/random.bed ${tmpdir}/random.hotspots.pass1.bed ${tmpdir}/random.hotspots.pass2.bed ${outdir}/random.${contig}.hotspots.both-passes.bed

# step 5: spot score (spot is calculated from the unthresholded hotspots combined from pass 1 and pass 2)

echo "${contig}: spot"

compute_spot ${tmpdir}/observed.bed ${outdir}/observed.${contig}.hotspots.both-passes.bed ${outdir}/observed.${contig}.spot.txt

