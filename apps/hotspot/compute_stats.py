#!/bin/env python


import sys, argparse

parser = argparse.ArgumentParser(prog ="compute_stats", description = "computes z-score and p-values for a list of hotspots")

parser.add_argument("--contig", action="store", required = True)
parser.add_argument("--mappable_genome", action="store", required = True)
parser.add_argument("--tag_file", action="store", required = True, help = "tag file (5' base only) in bed3 format")
parser.add_argument("--ignore_duplicates", action="store_true", help = "ignore duplicate sequencing tags")
parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help = "hotspot file (default: stdin)")

args = parser.parse_args()

contig = args.contig

#######################
# read mappable genome size
#######################

import subprocess

uniq_mappable_total = 0

proc = subprocess.Popen(["bedextract", contig, args.mappable_genome], stdout = subprocess.PIPE)

while True:

  line = proc.stdout.readline()
  
  if line == '': break
 
  (cont, start, end) = line.strip().split('\t')[:3]

  uniq_mappable_total += int(end) - int(start)

#######################
# read observed counts
#######################

import numpy as np

tags_count = {}

proc = subprocess.Popen(["bedextract", contig, args.tag_file], stdout = subprocess.PIPE)

while True:

	line = proc.stdout.readline()

	if line == '': break

	(cont, start, end) = line.strip().split('\t')[:3]

	tags_count[int(start)] = (tags_count.get(int(start), 0) + 1) if not args.ignore_duplicates else 1

tags_count_total = np.sum( tags_count.values() )

#######################
# read observed counts
#######################

import stats

for line in args.infile:

	(cont, left, right, name, start, end, tags_observed, uniq_mabbable, tags_in_window, uniq_mappable_in_window ) = line.strip().split('\t')

	# calc z-score

	(z_local, p_value_local) = stats.compute_z_score(int(tags_observed), int(uniq_mabbable), int(tags_in_window), int(uniq_mappable_in_window))

	(z_genome_wide, p_value_genome_wide) = stats.compute_z_score(int(tags_observed), int(uniq_mabbable), tags_count_total, uniq_mappable_total)
		
	if z_local < z_genome_wide:

		z = z_local
		p_value = p_value_local

	else:

		z = z_genome_wide
		p_value = p_value_genome_wide


	print "%s\t%0.4f\t%0.4f" % (line.strip(), z, p_value)