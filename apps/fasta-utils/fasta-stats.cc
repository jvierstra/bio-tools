#include <iostream>

#include "defs.hh"
#include "bio/alphabet/nucleotide.hh"
#include "bio/formats/fasta/istream.hh"
#include "misc/options.hh"

int main(int argc, const char *argv[]) {

	std::ios::sync_with_stdio(false);
	
	//initialize opts
	bool show_header = true, show_totals = true;
	
	misc::options::parser opts("fasta-stats", "calculate dna statistics from a fasta file", "< fasta_input");
	opts.add_bool_option('h', "no-header", "do not display header", show_header, "true", true, false);
	opts.add_bool_option('t', "no-total", "do not display totals", show_totals, "true", true, false);
	opts.parse(argv, argv + argc);
	
	nucleotide dna("ATCGN", "TAGCN");
	int A = dna.encode('A');
	int T = dna.encode('T');
	int C = dna.encode('C');
	int G = dna.encode('G');
	int N = dna.encode('N');

	count_t total_len = 0;
	count_t total_cpg = 0;
	std::vector<count_t> total_bases(dna.size());

	std::vector<count_t> bases(dna.size());

	io::fasta::istream input(std::cin);
	io::fasta::record rec;

	if(show_header) {
		//print header	
		std::cout << "seqname\tlen\tA\tT\tC\tG\tN\tcpg\tgc%\n";
	}

	while(input >> rec) {
		//init
		std::fill(bases.begin(), bases.end(), 0);
		count_t cpg = 0;
		int prev_base = -1;

		//count bases
		std::string::const_iterator c;
		for(c = rec.sequence.begin(); c != rec.sequence.end(); ++c) {
			int curr_base = dna.encode(*c);
			++bases[curr_base];
			if((prev_base == C) && (curr_base == G)) {
				++cpg;
			}
			prev_base = curr_base;
		}

		count_t total_count = bases[G] + bases[C] + bases[A] + bases[T];
		long double gc = (long double)(bases[G] + bases[C]) / (long double)total_count;

		//print information for sequence
		std::cout << rec.id << "\t" 
			<< rec.sequence.length() << "\t"
			<< bases[A] << "\t"
			<< bases[T] << "\t"
			<< bases[C] << "\t"
			<< bases[G] << "\t"
			<< bases[N] << "\t"
			<< cpg << "\t"
			<< gc << std::endl; 

		//update the totals
		total_len += rec.sequence.length();
		total_cpg += cpg;
		for(unsigned int i = 0; i < bases.size(); ++i) {
			total_bases[i] += bases[i];
		}
	}

	if (show_totals) {
		count_t total_count =  total_bases[G] + total_bases[C] + total_bases[A] + total_bases[T];
		long double total_gc = (long double)(total_bases[G] + total_bases[C]) / (long double)total_count;
		
		std::cout << "total" << "\t" 
			<< total_len << "\t"
			<< total_bases[A] << "\t"
			<< total_bases[T] << "\t"
			<< total_bases[C] << "\t"
			<< total_bases[G] << "\t"
			<< total_bases[N] << "\t"
			<< total_cpg << "\t"
			<< total_gc << std::endl; 
	}

	return 0;
}

