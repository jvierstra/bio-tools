DIR := apps/trim-adapters-illumina

LOCAL_SRCS := $(wildcard $(DIR)/*.cc)
LOCAL_HEADERS := $(wildcard $(DIR)/*.hh)
LOCAL_OBJS := $(LOCAL_SRCS:.cc=$(O))

LOCAL_MAINS := trim-adapters-illumina
LOCAL_MAINS_OBJS := $(foreach bin,$(LOCAL_MAINS),$(DIR)/$(bin)$(O))
LOCAL_SHARED_OBJS := $(filter-out $(LOCAL_MAINS_OBJS),$(LOCAL_OBJS))

LOCAL_BINS := $(foreach bin,$(LOCAL_MAINS),$(DIR)/$(bin)$(E))

$(LOCAL_BINS): $(LOCAL_SHARED_OBJS)
$(DIR): $(LOCAL_BINS) 

SRCS += $(LOCAL_SRCS)
BINS += $(LOCAL_BINS)

