// trim-adapter-illumina
// A utility to remove adapter sequences from pair-end illumina reads.

#include <cstdlib>
#include <iostream>
#include <map>

#include <omp.h>

#include "misc/options.hh"
#include "misc/string.hh"

#include "io/ifstream.hh"
#include "io/tabdelim/istream.hh"

#include "io/gz/ifstream.hh"
#include "io/gz/ofstream.hh"

#include "bio/formats/fastq/istream.hh"
#include "bio/formats/fastq/ostream.hh"

#include "bio/alphabet/nucleotide.hh"
#include "bio/alignment/smith_waterman_aligner.hh"

#define VERSION "0.1"

//main
int main(int argc, const char* argv[]) {
	
	std::ios::sync_with_stdio(false);

	int min_overlap = 2;
	float error_rate = 0.2;

	int num_threads = 1;

	std::string adapter_filepath, adapter1_name, adapter2_name;

	std::string read1_filepath, read2_filepath, trimmed_read1_filepath, trimmed_read2_filepath;

	misc::options::parser opts("trim-adapters-illumina", "trims adapter sequences using the smith-waterman algorithm from pair-end fastq files", "", VERSION); 

	opts.add_store_option('e', "error-rate", "error rate (pct.)", error_rate, to_string(error_rate), true);
	opts.add_store_option('m', "min", "minimum number of bases matching adapters", min_overlap, to_string(min_overlap), true);
	
	opts.add_store_option('@', "threads", "number of processor threads for parallel computing", num_threads, to_string(num_threads), true);
	
	opts.add_store_option('f', "adapter-file", "file with adapter sequences", adapter_filepath, "", false);
	opts.add_store_option('1', "adapter-1", "adapter 1 sequence name", adapter1_name, "", false);
	opts.add_store_option('2', "adapter-2", "adapter 2 sequence name", adapter2_name, "", false);

	opts.add_store_argument("<read 1>", "fastq.gz format", read1_filepath);
	opts.add_store_argument("<read 2>", "fastq.gz format", read2_filepath);
	opts.add_store_argument("<trimmed read 1>", "fastq.gz format", trimmed_read1_filepath);
	opts.add_store_argument("<trimmed read 2>", "fastq.gz format", trimmed_read2_filepath);
	
	opts.parse(argv, argv + argc);
	
	//setup number of threads for OpenMP
	omp_set_num_threads(num_threads);

	//setup the adapter sequence	
	io::ifstream adapter_file(adapter_filepath);
	io::tabdelim::istream adapter_stream(adapter_file);
	std::vector<std::string> toks;

	std::map<std::string, std::string> adapters;

	while(adapter_stream >> toks) {

		if( (*(toks.begin()))[0] != '#' ) {		
			adapters[*(toks.begin())] = *(toks.begin() + 1);
		}
	}

	adapter_file.close();
	
	//
	std::string adapter1, adapter2;

	try {

		adapter1 = adapters.at(adapter1_name);
		adapter2 = adapters.at(adapter2_name);

	} catch(std::out_of_range &e) {
		
		std::cerr << "Error: " << adapter1_name << " or " << adapter2_name << " not found in adapter file!" << std::endl;
		std::exit(1);
		
	}

	//read1 input gzip stream
	io::gz::ifstream read1_file(read1_filepath);
	io::fastq::istream read1_stream(read1_file);
	io::fastq::record read1;
	
	//read2 input gzip stream
	io::gz::ifstream read2_file(read2_filepath);
	io::fastq::istream read2_stream(read2_file);
	io::fastq::record read2;
	
	//read1 output stream
	io::gz::ofstream trimmed_read1_file(trimmed_read1_filepath);
	io::fastq::ostream trimmed_read1_stream(trimmed_read1_file);
		
	//read2 output stream
	io::gz::ofstream trimmed_read2_file(trimmed_read2_filepath);
	io::fastq::ostream trimmed_read2_stream(trimmed_read2_file);
	
	//start of my implementation of some basic stats
	unsigned long n = 0, ntrimmed = 0;
	
	//smith-waterman aligner; setup memory space
	int match = 3, mismatch = -1, gap = -2;

	smith_waterman_aligner<int> sw1(match, mismatch, gap, DNA);
	smith_waterman_aligner<int> sw2(match, mismatch, gap, DNA);
	smith_waterman_aligner<int> sw3(match, mismatch, gap, DNA);

	int dist1, dist2, dist3;
	
	pairwise_alignment alignment1, alignment2, alignment3;

	std::string read1_adapter, read2_adapter, expected_read1_adapter, expected_read2_adapter;
	size_t lena, lenb, enda, endb;

	while (read1_stream >> read1 and read2_stream >> read2) {

		lena = enda = read1.sequence.size();
		lenb = endb = read2.sequence.size();

		if (lena != lenb) throw std::runtime_error("");

		// Align read1 to read2 (revcomp)
		alignment1 = sw1.align(DNA.unmask(read1.sequence), DNA.reverse_complement(DNA.unmask(read2.sequence)));

		dist1 = (alignment1.mismatches + alignment1.gaps) + (alignment1.starta) + (lenb - alignment1.endb - 1);

		if(dist1 <= alignment1.enda * error_rate and 
			(alignment1.starta == lenb - alignment1.endb - 1)) {

			// Extract the putative adapter sequences from the sequencing read

			read1_adapter = read1.sequence.substr(alignment1.enda + 1);
			read2_adapter = read2.sequence.substr(lenb - alignment1.startb);
			
			// The read length might be longer than the adapter itself, and
			// in this case trim the putatitive adapter sequence to the length of the 
			// adapter.

			// Note: read1 will contain Adapter #2, while read2 will contain Adapter #1

			if (read1_adapter.size() > adapter2.size()) {
				read1_adapter = read1_adapter.substr(0, adapter2.size());
			} 
			if (read2_adapter.size() > adapter1.size()) {
				read2_adapter = read2_adapter.substr(0, adapter1.size());
			}
	
			// Now trim the adapters, as in many cases they are longer than the
			// putative adapter size.

			expected_read1_adapter = adapter2.substr(0, read1_adapter.size());
			expected_read2_adapter = adapter1.substr(0, read2_adapter.size());
		
			// If an exact match occurs set the trimming coordinates, else
			// perform two more local sequence alignments.

#pragma omp parallel sections default(shared)
{

#pragma omp section
{

			if (read1_adapter != expected_read1_adapter) {				

				alignment2 = sw2.align(DNA.unmask(read1_adapter), expected_read1_adapter);

				dist2 = expected_read1_adapter.size() - alignment2.matches;

				if (dist2 <= read1_adapter.length() * error_rate) {

					enda = alignment1.enda + 1 + alignment2.starta - alignment2.startb;

				}

			} else {

				enda = alignment1.enda + 1;
			}

}

#pragma omp section
{

			if (read2_adapter != expected_read2_adapter ) {

				alignment3 = sw3.align(DNA.unmask(read2_adapter), expected_read2_adapter);

				dist3 = expected_read2_adapter.size() - alignment3.matches;

				if (dist3 <= read2_adapter.length() * error_rate) {

					endb = lenb - alignment1.startb + alignment3.starta - alignment3.startb;
				}
			
			} else {

				endb = lenb - alignment1.startb;
			}
}

}
			
			if ( (enda != lena and lena - enda >= min_overlap) and (endb != lenb and lenb - endb >= min_overlap) ) {
				
				read1.sequence = read1.sequence.substr(0, enda);
                		read1.quality = read1.quality.substr(0, enda);
                     
                		read2.sequence = read2.sequence.substr(0, endb);
                		read2.quality = read2.quality.substr(0, endb);

				++ntrimmed;

			}
		}
		
		++n;

		// Write to output

		trimmed_read1_stream << read1;
		trimmed_read2_stream << read2;
		
	}

	//close up the files
	trimmed_read1_file.close();
	trimmed_read2_file.close();
	
	std::cerr << "Total read-pairs processed: " << n << "; Total read-pairs trimmed: " << ntrimmed << std::endl;

	std::exit(0);
}
