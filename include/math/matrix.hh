#ifndef _MATH_MATRIX_HH_
#define _MATH_MATRIX_HH_

#include <iostream>
#include <stdexcept>
#include <functional>
#include <algorithm>
#include <cmath>

#include <boost/lexical_cast.hpp>

#include "math/functions.hh"
#include "math/vector.hh"
#include "misc/string.hh"
#include "io/line/istream.hh"

namespace math {
		
	template<typename T = double>
	class matrix : public misc::basic_matrix<T> {
	public:
		
		//math functions
		virtual matrix operator+(const matrix& other) const {
			return matrix(*this) += other;
		}
		
		virtual matrix operator-(const matrix& other) const {
			return matrix(*this) -= other;
		}
		
		virtual matrix operator*(const matrix& other) const {
			return matrix(*this) *= other;
		}
		
		virtual matrix operator/(const matrix& other) const { 
			return matrix(*this) /= other; 
		}
		
		virtual matrix& operator+=(const matrix& other) {
			std::transform(this->begin(), this->end(), other.begin(), this->begin(), std::plus<T>());
			return *this;
		}
		
		virtual matrix& operator-=(const matrix& other) {
			std::transform(this->begin(), this->end(), other.begin(), this->begin(), std::minus<T>());
			return *this;
		}
		
		virtual matrix& operator*=(const matrix& other) {
			if(this->n == other.m) {
				pointer ndata = new value_type[this->m*other.n];
				for(size_type i = 0; i < this->m; ++i) {
					for(size_type j = 0; j < other.n; ++j) {
						for(size_type k = 0; k < this->n; ++k) {
							ndata[i*other.m + j] += this->operator()(i, k) * other.operator()(k, j); 
						}
					}
				}
				delete[] this->data;
				this->data = ndata;
				this->n = other.n;
			} else {
				throw std::runtime_error("matrix: cannot multiply matricies of given dimensions!");
			}
			return *this;
		}
		
		virtual matrix& operator/=(const matrix& other) {
			return *this;
		}
		
		virtual matrix operator+(const_reference other) const {
			return matrix(*this) += other;
		}
		
		virtual matrix operator-(const_reference other) const {
			return matrix(*this) -= other;
		}
		
		virtual matrix operator*(const_reference other) const {
			return matrix(*this) *= other;
		}
		
		virtual matrix operator/(const_reference other) const {
			return matrix(*this) /= other;
		}
		
		virtual matrix& operator+=(const_reference other) {
			std::for_each(this->begin(), this->end(), plus<T>(other));
			return *this;
		}
		
		virtual matrix& operator-=(const_reference other) {
			std::for_each(this->begin(), this->end(), plus<T>(-other));
			return *this;
		}
		
		virtual matrix& operator*=(const_reference other) {
			std::for_each(this->begin(), this->end(), multiplies<T>(other));
			return *this;
		}
		
		virtual matrix& operator/=(const_reference other) {
			std::for_each(this->begin(), this->end(), multiplies<T>(1/other));
			return *this;
		}
		
	};

}

#endif
