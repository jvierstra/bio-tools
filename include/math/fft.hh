#ifndef _FFT_HH_
#define _FFT_HH_

#include <cmath>

#include "functions.hh"

namespace math {
	namespace fft {
		
		template<typename T>
		inline void transform(std::vector<T>& data, int sign) {
			long i, j = 1, m, max, step, n;
			double wtemp, wr, wpr, wpi, wi, theta, rtemp, itemp;
			
			n = ((data.size() - 1) / 2) << 1;
			for(i = 1; i < n; i += 2) {
				if(j > i) {
					std::swap(data[j], data[i]);
					std::swap(data[j + 1], data[i + 1]);
				}
				m = n >> 1;
				while(m >= 2 and j > m) {
					j -= m;
					m >>= 1;
				}
				j += m;
			}
			
			max = 2;
			while(n > max) {
				step = max << 1;
				theta = sign * (2*math::pi / max);
				wtemp = std::sin(0.5 * theta);
				wpr = -2 * math::sqr(wtemp);
				wpi = std::sin(theta);
				wr = 1;
				wi = 0;	
				for(m = 1; m < max; m += 2) {
					for(i = m; i <= n; i += step) {
						j = i + max;
						rtemp = wr * data[j] - wi * data[j + 1];
						itemp = wr * data[j + 1] + wi * data[j];
						data[j] = data[i] - rtemp;
						data[j + 1] = data[i + 1] - itemp;
						data[i] += rtemp;
						data[i + 1] += itemp;
					}
					wr = (wtemp = wr) * wpr - wi * wpi + wr;
					wi = wi * wpr + wtemp * wpi + wi;
				}
				max = step;
			}
		}
		
	} //end fft namespace
} //end math namespace

#endif
