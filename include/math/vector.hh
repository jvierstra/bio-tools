#ifndef _VECTOR_HH_
#define _VECTOR_HH_

#include <iostream>
#include <stdexcept>
#include <functional>
#include <algorithm>
#include <cmath>

#include "math/functions.hh"

namespace math {
	
	template<typename T>
	class vector {
	public:
		typedef T value_type;
		typedef T *pointer;
		typedef const T *const_pointer;
   		typedef T &reference;
		typedef const T &const_reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		
		typedef T *iterator;
		typedef const T* const_iterator;
	public:
		vector(size_type n, value_type init = value_type()) : n(n) {
			data = new value_type[n];
			fill(init);
		};
		
		~vector() {
			delete[] data;
		}
		
		vector(const vector& other) {
			n = other.n;
			delete[] data;
			data = new value_type[n];
			std::copy(other.begin(), other.end(), begin());
		}
		
		vector& operator=(const vector& other) {
			if(this != &other) {
				n = other.n;
				delete[] data;
				data = new value_type[n];
				std::copy(other.begin(), other.end(), begin());
			}
		}
		
		const_reference operator[](size_type i) const { return data[i]; }
		reference operator[](size_type i) { return data[i]; }
		
		size_type size() const { return n; }
		void fill(const_reference val) { std::fill(begin(), end(), val); }
		void clear() { fill(value_type()); }
		
		iterator begin() { return &data[0]; }
		const_iterator begin() const { return &data[0]; }
		iterator end() { return &data[size()]; }
		const_iterator end() const { return &data[size()]; }
		
		const_reference operator()(size_type i) const { return data[i]; }
		reference operator()(size_type i) { return data[i]; }
		
		vector& operator/=(const_reference other) {
			std::for_each(this->begin(), this->end(), multiplies<T>(1/other));
			return *this;
		}
		
		vector& operator+=(const_reference other) {
			std::for_each(this->begin(), this->end(), plus<T>(other));
			return *this;
		}
		
		vector operator+(const T& val) const {
			return vector(*this) += val;	
		}
		
	protected:
		pointer data;
		size_type n;
	};
	
	template<typename T>
	inline T dot(const vector<T>& a, const vector<T>& b) {
		if(a.size() != b.size()) {
			throw std::runtime_error("dot: cannot calculate dot product of vectors of unequal sizes!");
		}
		
		T result = T(0);
		for (typename vector<T>::size_type i = 0; i < a.size(); ++i) {
			result += a(i) * b(i);
		}
		
		return result;
	}
	
	template<typename T>
	inline vector<T> cross(const vector<T>& a, const vector<T>& b) {}
}
#endif
