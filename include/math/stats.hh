#ifndef _STATS_HH_
#define _STATS_HH_

#include <numeric>

#include "vector.hh"
#include "matrix.hh"

namespace math {
	namespace stats {
		
		template<typename T>
		double mean(const vector<T>& data) {
			return double(std::accumulate(data.begin(), data.end(), T(0))) / double(data.size());
		}
		
		template<typename T>
		T variance(const vector<T>& data) {
			const T avg = mean(data);
			T sum = T(0);
			for (typename vector<T>::size_type i = 0; i < data.size(); ++i) {
				sum += sqr(data(i) - avg);
			}
			return (sum / T(data.size() - 1));
		}
		
		template<typename T>
		T mode(const vector<T>& data) {}
		
		template<typename T>
		T median(const vector<T>& data) {}
		
		template<typename T>
		T sd(const vector<T>& data) {
			return std::sqrt(variance(data));
		}
		
		template<typename T>
		vector<T> cumsum(const vector<T>& data) {
			vector<T> res(data.size());
			std::partial_sum(data.begin(), data.end(), res.begin());
			return res;
		}
			
		template<typename T>
		double correlation(const vector<T>& x, const vector<T>& y, int offset = 0) {
			double mean_x = mean<T>(x);
			double mean_y = mean<T>(y);
			
			if(x.size() != y.size()) {
				throw std::runtime_error("correlation: vectors must be of equal length!");
			} 
			
			double ss_xx = 0, ss_yy = 0, ss_xy = 0;
			int n = x.size(); 
			for(int i = offset; i < n - offset; ++i) {
				ss_xx += sqr(double(x[i+offset]) - mean_x);
				ss_yy += sqr(double(y[i-offset]) - mean_y);
				ss_xy += (double(x[i+offset]) - mean_x)  * (double(y[i-offset]) - mean_y);
				//std::cout << i << "\t" << ss_xx << "\t" << ss_yy << "\t" << ss_xy << std::endl;
			}
					
			return (ss_xy * ss_xy) / (ss_xx * ss_yy);
		}
		
		template<typename T>
		matrix<T> pearsons(const math::matrix<T>& data) {
			const typename matrix<T>::size_type m = data.rows();
			const typename matrix<T>::size_type n = data.cols();
			
			//calculate means
			vector<T> means(m);
			for (typename vector<T>::size_type i = 0; i < n; ++i) {
				for (typename vector<T>::size_type j = 0; j < m; ++j) {
					means(j) += data(i, j);
				}
			}
			means /= T(n);
			
			//define covariance matrix
			matrix<T> cor(m, m);
			for (typename matrix<T>::size_type i = 0; i < m; ++i) {
				for (typename matrix<T>::size_type j = 0; j < i + 1; ++j) {
					T sum = T(0), sdx = T(0), sdy = T(0);
					for (typename matrix<T>::size_type k = 0; k < n; ++k) {
						sdx += sqr(data(k, i) - means(i));
						sdy += sqr(data(k, j) - means(j));
						sum += data(k, i) * data(k, j);
					}
					
					sdx = std::sqrt(sdx / T(n - 1));
					sdy = std::sqrt(sdy / T(n - 1));
					
					cor(i, j) = (sum - means(i) * means(j) * T(n));
					cor(i, j) /= (T(n) - T(1)) * sdx * sdy;
					cor(j, i) = cor(i, j);					
				}
			}
			return cor;
		}
	}
}
#endif
