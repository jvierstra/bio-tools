#ifndef LOGDOUBLE_HH_
#define LOGDOUBLE_HH_

#include <stdexcept>
#include <cmath>
#include <istream>
#include <ostream>

class logdouble {
public:
  logdouble(double val = 0.0);

  logdouble& operator*=(const logdouble& other);
  logdouble& operator+=(const logdouble& other);
  logdouble& operator/=(const logdouble& other);
  logdouble& operator-=(const logdouble& other);

  logdouble operator*(const logdouble& other) const;
  logdouble operator+(const logdouble& other) const;
  logdouble operator/(const logdouble& other) const;
  logdouble operator-(const logdouble& other) const;
  logdouble& operator^=(const double& exponent);
  logdouble operator^(const double& exponent) const;
  
  bool operator==(const logdouble& other) const;
  bool operator>(const logdouble& other) const;
  bool operator<(const logdouble& other) const;
  bool operator<=(const logdouble& other) const;
  bool operator>=(const logdouble& other) const;

  operator double() const { return std::exp(value); }

  double value;

private:
  static double log_one_plus_exp(double x);
  static double log_one_minus_exp(double x);
  static double derivative_log_one_plus_exp(double x);
  //5-7-08 Removed linear interpolator
  //static PiecewiseLinearInterpolator log_one_plus_exp_approx;
  //static PiecewiseCubicHermiteInterpolator log_one_plus_exp_approx2;
};

inline logdouble::logdouble(double val)
  : value(std::log(val)) {
}

inline logdouble& logdouble::operator*=(const logdouble& other) {
  value += other.value;
  return *this;
}

inline logdouble& logdouble::operator/=(const logdouble& other) {
  value -= other.value;
  return *this;
}

inline logdouble& logdouble::operator^=(const double& exponent) {
  value *= exponent;
 return *this;
}
	
inline logdouble logdouble::operator*(const logdouble& other) const {
  return logdouble(*this) *= other;
}

inline logdouble logdouble::operator+(const logdouble& other) const {
  return logdouble(*this) += other;
}

inline logdouble logdouble::operator/(const logdouble& other) const {
  return logdouble(*this) /= other;
}

inline logdouble logdouble::operator-(const logdouble& other) const {
  return logdouble(*this) -= other;
}	

inline logdouble logdouble::operator^(const double& exponent) const {
  return logdouble(*this) ^= exponent;
}
	
inline bool logdouble::operator==(const logdouble& other) const {
  return value == other.value;
}

inline bool logdouble::operator<(const logdouble& other) const {
  return value < other.value;
}

inline bool logdouble::operator>(const logdouble& other) const {
  return value > other.value;
}

inline bool logdouble::operator>=(const logdouble& other) const {
  return value >= other.value;
}

inline bool logdouble::operator<=(const logdouble& other) const {
  return value <= other.value;
}

inline std::istream& operator>>(std::istream& stream, logdouble& ld) {
  return stream >> ld.value;
}

inline std::ostream& operator<<(std::ostream& stream, const logdouble& ld) {
  return stream << ld.value;
}

#endif /*LOGDOUBLE_HH_*/
