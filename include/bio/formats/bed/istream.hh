#ifndef _BED_ISTREAM_H_
#define _BED_ISTREAM_H_

#include <vector>
#include <stdexcept>
#include <iosfwd>

#include <boost/lexical_cast.hpp>

#include "record.hh"
#include "utils/io/tabdelim/istream.hh"

namespace io { namespace bed {

	class istream {
	public:
		istream(std::istream& stream, const size_t buffer_size = 4 * 1024);
		istream& operator>>(record& rec);
		
		operator bool() const;
		bool operator!() const;
		
	protected:
		io::tabdelim::istream stream;
		std::vector<std::string> toks;
	};
	
	inline istream::istream(std::istream& stream, const size_t buffer_size)
	: stream(stream, buffer_size) {
	}
	
	inline istream::operator bool() const {
		return stream;
	}
	
	inline bool istream::operator!() const {
		return !stream;
	}
	
	
	inline istream& istream::operator>>(record& rec) {
		
		if(!stream) {
			return *this;
		}
		
		stream >> toks;

		try {
			rec.chrom = boost::lexical_cast<std::string>(toks[0]);
			rec.start = boost::lexical_cast<long>(toks[1]);
			rec.end = boost::lexical_cast<long>(toks[2]);
		} catch (boost::bad_lexical_cast& e) {
			throw std::runtime_error("malformed bed file: data types are inconsistent with file definition!");
		} catch (std::out_of_range& e) {
			throw std::runtime_error("malformed bed file: line has too few elements!");
		}
		return *this;
	}
		
} }

#endif
