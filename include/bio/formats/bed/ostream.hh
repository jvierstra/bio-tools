#ifndef _BED_OSTREAM_HH_
#define _BED_OSTREAM_HH_

#include "record.hh"

namespace io { namespace bed {
	
	class ostream {
	public:
		ostream(std::ostream& stream) : stream(stream) {}
		
		ostream& operator<<(const record& rec) {
			stream << rec.chrom << "\t" << rec.start << "\t" << rec.end <<std::endl;
			return *this;
		}
		
	protected:
		std::ostream& stream;
	};
	
} }

#endif
