#ifndef _FASTQ_CONSTANTS_HH_
#define _FASTQ_CONSTANTS_HH_

namespace io { namespace fastq {
	static const size_t DEFAULT_LINE_WIDTH = 60;
	static const char IDENTIFIER_LINE_PREFIX = '@';
	static const char QUALITY_LINE_PREFIX = '+';
} }

#endif
