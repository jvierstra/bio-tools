#ifndef _FASTQ_ISTREAM_HH_
#define _FASTQ_ISTREAM_HH_

#include <vector>
#include <string>
#include <iosfwd>
#include <istream>
#include <algorithm>

#include "constants.hh"
#include "record.hh"

#include "misc/string.hh"

using namespace misc::string;

namespace io { namespace fastq {
	
	class istream {
	public:
		istream(std::istream& stream, const size_t buffer_len = (4 * 1024));
		
		istream& operator>>(record& rec);  
		
		operator bool() const;
		bool operator!() const;
		
	protected:
		void fill_buffer();
		
	protected:
		std::istream& stream;
		std::vector<char> buffer;
		std::vector<char>::iterator pos;
		bool end;
		bool whitespace;
	};
	
	
	inline istream::istream(std::istream& stream, const size_t buffer_len) 
	: stream(stream), buffer(buffer_len), pos(buffer.end()), whitespace(false) {
		end = false; 
		
		while(stream && pos == buffer.end()) {
			fill_buffer();
			pos = std::find(buffer.begin(), buffer.end(), IDENTIFIER_LINE_PREFIX);
		}
		if(pos == buffer.end()) {
			end = true;
		}
	}
	
	inline istream::operator bool() const {
		return !end;
	}
	
	inline bool istream::operator!() const {
		return end;
	}
	
	inline void istream::fill_buffer() {
		stream.read(&buffer[0], buffer.size());
		if(static_cast<size_t>(stream.gcount()) < buffer.size()) {
			buffer.resize(stream.gcount());
		}
	}
	
	inline istream& istream::operator>>(record& rec) {
		rec.identifier = "";
		rec.sequence = "";
		rec.quality = "";
		
		if(pos == buffer.end()) {
			end = true;
			return *this;
		}
		
		//read record identifier
		std::vector<char>::iterator start = pos + 1;
		pos = std::find(start, buffer.end(), '\n');
		while(stream && pos == buffer.end()) {
			rec.identifier.append(start, pos);
			fill_buffer();
			start = buffer.begin();
			pos = std::find(start, buffer.end(), '\n');
		}
		
		if (start < pos) {
			rec.identifier.append(start, pos);
		}
		rec.identifier = strip_right(rec.identifier);
		
		//read sequence
		start = pos + 1;
		pos = std::find(start, buffer.end(), '\n');
		while(stream && pos == buffer.end()) {
			rec.sequence.append(start, pos);
			fill_buffer();
			start = buffer.begin();
			pos = std::find(start, buffer.end(), '\n');
		}
		
		if (start < pos) {
			rec.sequence.append(start, pos);
		}
		rec.sequence = strip_right(rec.sequence);
		
		//skip the quality header line
		start = pos + 1;
		pos = std::find(start, buffer.end(), '\n');
		while(stream && pos == buffer.end()) {
			fill_buffer();
			start = buffer.begin();
			pos = std::find(start, buffer.end(), '\n');
		}
		
		//read quality line
		start = pos + 1;
		pos = std::find(start, buffer.end(), '\n');
		while(stream && pos == buffer.end()) {
			rec.quality.append(start, pos);
			fill_buffer();
			start = buffer.begin();
			pos = std::find(start, buffer.end(), '\n');
		}
		
		if (start < pos) {
			rec.quality.append(start, pos);
		}
		rec.quality = strip_right(rec.quality);
		
		//find the the next record
		start = pos + 1;
		pos = std::find(pos+1, buffer.end(), IDENTIFIER_LINE_PREFIX);
		while(stream && pos == buffer.end()) {
			fill_buffer();
			start = buffer.begin();
			pos = std::find(start, buffer.end(), IDENTIFIER_LINE_PREFIX);
		}
		
		return *this;
	}
	
}	}
#endif
