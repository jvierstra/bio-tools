#ifndef _FASTA_RECORD_HH_
#define _FASTA_RECORD_HH_

namespace io { namespace fasta {

	static const size_t DEFAULT_LINE_WIDTH = 60;
	static const char TITLE_LINE_PREFIX = '>';

	struct record {
		record(const std::string& id = "", const std::string& sequence = "")
		: id(id), sequence(sequence) {};

		std::string id;
		std::string sequence;
	};

} }

#endif
