#ifndef _SMITH_WATERMAN_AFFINE_ALIGNER_HH_
#define _SMITH_WATERMAN_AFFINE_ALIGNER_HH_

#include "smith_waterman_aligner.hh"
#include "math/functions.hh"

using namespace utils;
using namespace math;

template<typename T = double>
class smith_waterman_affine_aligner : public smith_waterman_aligner<T> {
protected:
	T space;
	dpmatrix<T> Ia, Ib;
public:
	smith_waterman_affine_aligner(T match, T mismatch, T gap, T space, const alphabet& alpha)
	: smith_waterman_aligner<T>(match, mismatch, gap, alpha), space(space) {}

	virtual pairwise_alignment align(const std::string& a, const std::string& b) {

		if ((a.length() != this->H.rows() - 1) or (b.length() != this->H.cols() - 1)) {
                       	this->H = dpmatrix<T>(a.length() + 1, b.length() + 1);
			Ia = dpmatrix<T>(a.length() + 1, b.length() + 1, T(0));
			Ib = dpmatrix<T>(a.length() + 1, b.length() + 1, T(0));
		}

		//initialize
		for(int i = 0; i < a.length()+1; ++i) Ia(i, 0).value = Ib(i, 0).value = -std::numeric_limits<T>::infinity();
		for(int j = 0; j < b.length()+1; ++j) Ia(0, j).value = Ib(0, j).value = -std::numeric_limits<T>::infinity();
		
		T zero = T(0);	
		T h[3], ia[2], ib[2], score, m = zero;

		for (int i = 1; i < a.length()+1; ++i) {
			for (int j = 1; j < b.length()+1; ++j) {
				//match/mismatch score
				std::cout << a[i-1] << " " << int(this->alpha.encode(a[i-1])) << " " << b[j-1] << " " << int(this->alpha.encode(b[j-1])) 
					<<" " << int(this->alpha.encode(a[i-1]) & this->alpha.encode(b[j-1])) << std::endl;
				if(this->alpha.is_equal(a[i-1], b[j-1])) { std::cout << "E" << std::endl; }			
	
				score = (this->alpha.is_equal(a[i-1], b[j-1])) ? this->match : this->mismatch;
				
				//match matrix			
				h[0] = this->H(i-1, j-1).value + score;
				h[1] = Ia(i-1, j-1).value + score;
				h[2] = Ib(i-1, j-1).value + score;
				m = this->H(i, j).value = max<T, 4>(zero, h[0], h[1], h[2]);
				
				if(m == h[0]) this->H(i, j).trace = &this->H(i-1, j-1);
				else if(m == h[1]) this->H(i, j).trace = &Ia(i-1, j-1);
				else if(m == h[2]) this->H(i, j).trace = &Ib(i-1, j-1);
								
				//insertion
				ia[0] = this->H(i, j-1).value + this->gap + this->space;
				ia[1] = Ia(i, j-1).value + this->space;
				m = Ia(i, j).value = max<T, 2>(ia[0], ia[1]);
				
				if(m == ia[0]) Ia(i, j).trace = &this->H(i, j-1);
				else if(m == ia[1]) Ia(i, j).trace = &Ia(i, j-1);

				//deletion
				ib[0] = this->H(i-1, j).value + this->gap + this->space;
				ib[1] = Ib(i-1, j).value + this->space;
				m = Ib(i, j).value = max<T, 2>(ib[0], ib[1]);

				if(m == ib[0]) Ib(i, j).trace = &this->H(i-1, j);
				else if(m == ib[1]) Ib(i, j).trace = &Ib(i-1, j);
			}
		}
		
		dpcell<T>* cell = std::max_element(this->H.begin(), this->H.end());
		
		pairwise_alignment alignment;
		
		alignment.score = double(cell->value);
		alignment.enda = cell->i-1;
		alignment.endb = cell->j-1;
				
		while(cell->trace) {
			if(cell->i == cell->trace->i+1 && cell->j == cell->trace->j+1)	{
				alignment.a += a[cell->i-1];
				alignment.b += b[cell->j-1];
				alignment.mismatches += this->alpha.is_equal(a[cell->i-1], b[cell->j-1]) ? 0 : 1;
			}	else if (cell->i == cell->trace->i) {
				alignment.a += '-';
				alignment.b += b[cell->j-1];
				alignment.gaps++;
			} else if (cell->j == cell->trace->j) {
				alignment.a += a[cell->i-1];
				alignment.b += '-';
				alignment.gaps++;
			}
			cell = cell->trace;
		}

		alignment.starta = cell->i;
		alignment.startb = cell->j;
		alignment.reverse();
		
		return alignment;
	}
};

#endif
