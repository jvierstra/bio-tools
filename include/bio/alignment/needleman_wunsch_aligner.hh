#ifndef _NEEDLEMAN_WUNSCH_ALINGER_HH_
#define _NEEDLEMAN_WUNSCH_ALINGER_HH_

#include <new>

#include "pairwise_sequence_aligner.hh"
#include "score_matrix.hh"
#include "misc/dpmatrix.hh"

using namespace misc;

template<typename T = double>
class needleman_wunsch_aligner : public pairwise_sequence_aligner<T> {
public:
	needleman_wunsch_aligner() {}
	needleman_wunsch_aligner(const score_matrix<T>& sim, T gap) : sim(sim), gap(gap) {}
	
	virtual pairwise_alignment align(const std::string& a, const std::string& b) {		
		
		pairwise_alignment alignment;

		try {

			dpmatrix<T> h(a.length()+1, b.length()+1);
	
			//initialize
			for(size_t i = 0; i < a.length()+1; ++i) h(i, 0).value = i * gap;
			for(size_t j = 0; j < b.length()+1; ++j) h(0, j).value = j * gap;
			
			for (size_t i = 1; i < a.length()+1; ++i) {
				for (size_t j = 1; j < b.length()+1; ++j) {
					T left = h(i, j-1).value + gap;
					T diag = h(i-1, j-1).value + sim.get_char_score(a[i-1], b[j-1]);
					T up = h(i-1, j).value + gap;

					T max = h(i, j).value = std::max(left, std::max(diag, up));
					if(max == left) h(i, j).trace = &h(i, j-1);
					if(max == diag) h(i, j).trace = &h(i-1, j-1);
					if(max == up) h(i, j).trace = &h(i-1, j);
				}
			}

			dpcell<T>* cell = &h(a.length(), b.length());

			alignment.score = cell->value;
			
			while(cell->trace) {
				if(cell->i == cell->trace->i+1 && cell->j == cell->trace->j+1)	{
					alignment.a += a[cell->i-1];
					alignment.b += b[cell->j-1];
					alignment.mismatches += this->sim.alpha.is_equal(a[cell->i-1], b[cell->j-1]) ? 0 : 1;
				}	else if (cell->i == cell->trace->i) {
					alignment.a += '-';
					alignment.b += b[cell->j-1];
					alignment.gaps++;
				} else if (cell->j == cell->trace->j) {
					alignment.a += a[cell->i-1];
					alignment.b += '-';
					alignment.gaps++;
				}
				cell = cell->trace;
			}

			alignment.starta = 0;
			alignment.enda = a.length()-1;
			alignment.startb = 0;
			alignment.endb = b.length()-1;
		
			alignment.reverse();

		} catch(std::bad_alloc& e) {
			std::cerr << e.what() << std::endl;
		}

		

		return alignment;
	}
	
private:
	const score_matrix<T>& sim;
	T gap;
};

#endif
