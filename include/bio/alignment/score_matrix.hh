#ifndef _SCORE_MATRIX_HH_
#define _SCORE_MATRIX_HH_

#include "misc/matrix.hh"
#include "bio/alphabet/alphabet.hh"
#include "bio/alphabet/nucleotide.hh"

/*
A score matrix for sequence alignment
*/

template<typename T = double>
class score_matrix : public misc::basic_matrix<T> {
public:
	score_matrix() {}
	score_matrix(size_t n, size_t m)
	: basic_matrix<T>(n, m) {}
	score_matrix(const alphabet& a)
	: basic_matrix<T>(a.size(), a.size()), alpha(a) {
	}
	
	T get_char_score(char a, char b) const {
		return this->operator()(alpha.encode(a), alpha.encode(b));
	}

	virtual void set_char_score(char a, char b, T val) {
		this->operator()(alpha.encode(a), alpha.encode(b)) = val;
	}

	virtual void set_match_score(T val) {
		for(size_t i = 0; i < alpha.size(); ++i) {
			this->operator()(i, i) = val;
		}
	}

	virtual void set_mismatch_score(T val) {
		for(size_t i = 0; i < alpha.size(); ++i) {
			for(size_t j = i+1; j < alpha.size(); ++j) {
				this->operator()(i, j) = val;
				this->operator()(j, i) = val;
			}
		}
	}

	virtual void set_symmetric_score(char a, char b, T val) {
		set_char_score(a, b, val);
		set_char_score(b, a, val);
	}

	const alphabet& alpha;
};

template<typename T>
class dna_score_matrix : public score_matrix<T> {
public:
	dna_score_matrix() : score_matrix<T>(DNA) {}
	dna_score_matrix(T match, T mismatch) : score_matrix<T>(DNA) {
		set_match_score(match);
		set_mismatch_score(mismatch);
	}
};

#endif
