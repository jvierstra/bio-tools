#ifndef _PAIRWISE_SEQUENCE_ALIGNER_HH_
#define _PAIRWISE_SEQUENCE_ALIGNER_HH_

#include "pairwise_alignment.hh"

#include "../alphabet/alphabet.hh"

template<typename T = double>
class pairwise_sequence_aligner {
public:
	pairwise_sequence_aligner() {}

	virtual ~pairwise_sequence_aligner() {}
	virtual pairwise_alignment align(const std::string& a, const std::string& b) = 0;
};

#endif

