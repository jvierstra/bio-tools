#ifndef _TRANSLATION_HH_
#define _TRANSLATION_HH_

#include <string>
#include <stdexcept>
#include <vector>

#include "bio/alphabet/alphabet.hh"
#include "utils/string.hh"

class table {
public:
	table(const char* tab, unsigned int num, std::string name, std::string name2)
	: tab(tab), num(num), name(name), name2(name2) {
		if(num >= tables.size()) {
			tables.resize(num+1, NULL);
		}
		tables[num] = this;
	}
	
	static const table* get(unsigned int num) {
		return num >= tables.size() ?  NULL : tables[num];
	}
private:
	static std::vector<const table*> tables;
	static const encoder enc;

	const char* tab;
	unsigned int num;
	std::string name, name2;
public:
	std::string translate(const std::string& seq, const unsigned int phase, const bool stop) const {
		size_t size = (seq.size() < phase ? 0 : (seq.size() - phase) / 3);
		std::string res(size, '?');
		for(size_t i = 0; i < size; ++i) {
			size_t pos = i * 3 + phase;
			char aa = translate_codon(seq[pos], seq[pos+1], seq[pos+2]);
			if(stop && aa == '*') {
				res.resize(i);
				break;
			} else {
				res[i] = aa;
			}
		} 
		return res;
	}

	char translate_codon(const char base1, const char base2, const char base3) const {
		char aa = tab[(enc(base1) << 8) + (enc(base2) << 4) + (enc(base3))];
		return aa;
	}

	bool start(const char base1, const char base2, const char base3) const {
		if(num != 1) throw std::runtime_error("translator: start codons only pertain to the default translation table!");
		char first = std::toupper(base1);
		return std::toupper(base2) == 'T' && std::toupper(base3) == 'G' &&
			(first == 'A' || first == 'C' || first == 'T'
				|| first == 'Y' || first == 'M' || first == 'W' || first == 'H'); 
	}
	bool start(const std::string& triplet) const {
		return start(triplet[0], triplet[1], triplet[2]);
	}
	bool stop(const char base1, const char base2, const char base3) const {
		return translate_codon(base1, base2, base3) == '*';
	}
	bool stop(const std::string& triplet) const {
		return stop(triplet[0], triplet[1], triplet[2]);
	}
};

const encoder table::enc("ACGTUNMRWSYKVHDB", false, 'N');
std::vector<const table*> table::tables = std::vector<const table*>();

#include "translation.tables"

class translator {
public:
	translator(unsigned int num = 1) 
	: tab(table::get(num)) {
		if(!tab) {
			throw std::runtime_error("translator: no translation table numbered: " + utils::string::to_string(num));
		}
	}
	
	translator(const table* tab) : tab(tab) {};
	
	std::string operator()(const std::string& seq) const { return translate(seq); }
	
	std::string translate(const std::string& seq, const unsigned int phase = 0, const bool stop = false) const {
		return tab->translate(seq, phase, stop);
	}
	
	char translate(const char base1, const char base2, const char base3) const {
		return tab->translate_codon(base1, base2, base3);
	}
	
	std::vector<std::string> translate_orfs(const std::string& seq) const {
		size_t size = seq.size();
		std::vector<size_t> starts;
		
		std::vector<std::string> orfs;
		
		for(size_t i = 0; i < size - 3; ++i) {
			if(tab->start(seq[i], seq[i+1], seq[i+2])) {
				starts.push_back(i + 3);
			}
		}
		
		std::vector<size_t>::const_iterator start;
		for(start = starts.begin(); start != starts.end(); ++start) {
			orfs.push_back(translate(seq.substr(*start)));
		}
		return orfs;
	}
						 
private:
	const table* tab;
};


class translation 
{
public:
	translation(const std::string& dna, const std::string& aa);
};
#endif
