/*
 name: motif.hh
 description: motif model based (position-weight matrix)
 author: jeff vierstra
 date-added:
 date-modified:
*/

#ifndef _MOTIF_HH_
#define _MOTIF_HH_

#include <string>
#include <cmath>

#include "math/matrix.hh"
#include "bio/alphabet/nucleotide.hh"

template<typename T = double>
class motif : public math::matrix<T> {
public:
	typedef typename math::matrix<T>::size_type size_type;
	
public:
	motif() {}
	motif(size_type n) : math::matrix<T>(4, n) {}
	
	size_type size() const { return this->n; }
	
	void normalize();
	void pseudocount(const T& val);
	
	std::string consensus() const;
	double entropy(size_type i) const;
	double entropy() const;
	
	T likelihood(const std::string& sequence, bool rc = false) const;
};

template<typename T>
inline std::string motif<T>::consensus() const {
	std::string res;
	for (size_type j = 0; j < this->n; ++j) {
		unsigned int map = 0, matches = 0;
		T max = T(0);
		
		for (size_type i = 0; i < 4; ++i) {
			if (this->operator()(i, j) > max) {
				max = this->operator()(i, j);
			}		
		}
		const T ratio = max / 2.0;
		for (size_type i = 0; i < 4; ++i) {
			if (this->operator()(i, j) >= ratio) {
				map |= (1 << i);
				matches++;
			}
		}
		res += (matches > 1) ? std::tolower(IUPAC_DNA.decode(map)) : IUPAC_DNA.decode(map);
	}
	return res;
}

template<typename T>
inline T motif<T>::likelihood(const std::string& sequence, bool rc) const {
	T res = T(0);
	
	if (sequence.size() != size()) {
		throw std::runtime_error("motif::likelihood: sequence length does not match motif width!");
	}
	
	for (std::string::size_type j = 0; j < sequence.size(); ++j) {
		size_type base = rc ? size() - 1 - j : j;
		const size_t i = DNA.encode(rc ? DNA.complement(sequence[base]) : sequence[base]);
		
		if(i < 4) {		
			res *= this->operator()(i, j);
		} else {
			//throw exception
		}
	}
	
	return res;
}

template<typename T>
inline void motif<T>::normalize() {
	for (size_type j = 0; j < this->n; ++j) {
		T mass = this->operator()(0, j) + this->operator()(1, j) + this->operator()(2, j) + this->operator()(3, j);
		if (mass > T(0)) {
			this->operator()(0, j) /= mass;
			this->operator()(1, j) /= mass;
			this->operator()(2, j) /= mass;
			this->operator()(3, j) /= mass;
		}
	}
}

template<typename T>
inline void motif<T>::pseudocount(const T& val) {
	*this += val;
}


template<typename T>
inline double motif<T>::entropy(size_type j) const {
	double res = 0;
	for (size_type i = 0; i < 4; ++i) {
		const double prob = double(this->operator()(i, j));
		if (prob > 0) {
			res += -prob * log2(prob);
		}
	}
	return res;
}

template<typename T>
inline double motif<T>::entropy() const {
	double res = 0;
	for (size_type j = 0; j < this->n; ++j) {
		res += entropy(j);
	}
	return res;
}

#endif
