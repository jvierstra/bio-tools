#ifndef _NUCLEOTIDE_HH_
#define _NUCLEOTIDE_HH_

#include <string>
#include <algorithm>

#include "alphabet.hh"

class nucleotide : public virtual alphabet {
public:
	nucleotide(const std::string chars, const std::string complement);
	
	unsigned int size() const;
	bool is_member(const char c) const;
	virtual bool is_equal(const char a, const char b) const;
	
	unsigned char encode(const char base) const;
	char decode(const unsigned char c) const;

	std::string reverse(std::string s) const;
	
	char complement(const char base) const;
	std::string complement(std::string s) const;
	std::string reverse_complement(std::string s) const;
	
	std::string transcribe(std::string s) const;
	std::string reverse_transcribe(std::string s) const;
	
	std::string nib_encode(const std::string& s) const;
	std::string nib_decode(const std::string& s) const;
	
	std::string hard_mask(std::string s) const;
	std::string unmask(std::string s) const;
	
private:
	const decoder dec;
	const encoder enc;
	const mapper comp;
	
	static const mapper trans;
	static const mapper revtrans;
	
	static const mapper hard_masker;
	static const mapper unmasker;
};

inline nucleotide::nucleotide(const std::string chars, const std::string complement)
: dec(chars), enc(chars, false), comp(chars, complement, false) {
}

inline char nucleotide::complement(const char base) const {
	return comp(base);
}

inline unsigned int nucleotide::size() const {
	return dec.table.size();
}

inline bool nucleotide::is_member(const char c) const {
	return enc.is_member(c);
}

inline bool nucleotide::is_equal(const char a, const char b) const {
	return a == b;
}

inline unsigned char nucleotide::encode(const char base) const {
	return enc(base);
}

inline char nucleotide::decode(const unsigned char c) const {
	return dec(c);
}

inline std::string nucleotide::hard_mask(std::string s) const {
	std::transform(s.begin(), s.end(), s.begin(), hard_masker);
	return s;
}

inline std::string nucleotide::unmask(std::string s) const {
	std::transform(s.begin(), s.end(), s.begin(), unmasker);
	return s;
}

inline std::string nucleotide::reverse(std::string s) const {
	std::reverse(s.begin(), s.end());
	return s;
}

inline std::string nucleotide::complement(std::string s) const {
	std::transform(s.begin(), s.end(), s.begin(), comp);
	return s;
}

inline std::string nucleotide::reverse_complement(std::string s) const {
	std::reverse(s.begin(), s.end());
	return this->complement(s);
}

inline std::string nucleotide::transcribe(std::string s) const {
	std::transform(s.begin(), s.end(), s.begin(), trans);
	return s;
}

inline std::string nucleotide::reverse_transcribe(std::string s) const {
	std::transform(s.begin(), s.end(), s.begin(), revtrans);
	return s;
}

inline std::string nucleotide::nib_encode(const std::string& s) const {
	unsigned int length = s.size() % 2 ? (s.size() / 2) + 1 : s.size() / 2;
	std::string encoded(length, 0);
	for(unsigned int i = 0; i < s.size(); i += 2) {
		unsigned char upper = encode(s[i]);
		unsigned char lower = ((i + 1) < s.size()) ? encode(s[i + 1]) : 0x0F;
		encoded[i / 2] = (upper << 4) | lower; 
	}
	return encoded;
}

inline std::string nucleotide::nib_decode(const std::string& s) const {
	unsigned int length;
	if(s.size() > 0 && (s[s.size() - 1] & 0x0F) == 0x0F) {
		length = (s.size() * 2) - 1;
	} else {
		length = s.size() * 2;
	}
	std::string decoded(length, '?');
	for(unsigned int i = 0; i < length; i += 2) {
		decoded[i] = decode((s[i / 2] >> 4) & 0x0F);
	}
	for(unsigned int i = 1; i < length; i += 2) {
		decoded[i] = decode(s[i / 2] & 0x0F);
	}
	return decoded;
}

const mapper nucleotide::trans("T", "U", false);
const mapper nucleotide::revtrans("U", "T", false);

const mapper nucleotide::hard_masker("abcdefghijklmnopqrstuvwxyz", "NNNNNNNNNNNNNNNNNNNNNNNNNN");
const mapper nucleotide::unmasker("abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

const static nucleotide DNA("ACGTN", "TGCAN");
const static nucleotide RNA("ACGU", "UCGA");

const static nucleotide PURINES("AG", "CT");
const static nucleotide PYRIMIDINES("TC", "AG");

//moved to deneragate_nucleotide.hh
//const static nucleotide IUPAC_DNA("XACMGRSVTWYHKDBN","XTGNCYSBAWYDMHVN");
//const static nucleotide IUPAC_RNA("", "");

/*
   TGCA
 X 0000 0
 A 0001 1
 C 0010 2
 M 0011 3
 G 0100 4
 R 0101 5
 S 0110 6
 V 0111 7
 T 1000 8
 W 1001 9
 Y 1010 10
 H 1011 11
 K 1100 12
 D 1101 13
 B 1110 14
 N 1111 15
 */

#endif

