#ifndef _DEGENERATE_NUCLEOTIDE_HH_
#define _DEGENERATE_NUCLEOTIDE_HH_

#include "degenerate_alphabet.hh"
#include "nucleotide.hh"

class degenerate_nucleotide : public nucleotide, public degenerate_alphabet {
public:
	degenerate_nucleotide(const std::string chars, const std::string complement)
	: nucleotide(chars, complement) {}

	bool is_equal(const char a, const char b) const {
		return ((encode(a) & encode(b)) == encode(a));
	}

	std::string get_possible_chars(const char c) const { return std::string(""); }	
};

const static degenerate_nucleotide IUPAC_DNA("XACMGRSVTWYHKDBN","XTGNCYSBAWYDMHVN");

#endif
