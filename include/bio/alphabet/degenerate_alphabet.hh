#ifndef _DEGENERATE_ALPHABET_HH_
#define _DEGENERATE_ALPHABET_HH_

#include <string>

#include "alphabet.hh"

class degenerate_alphabet : public virtual alphabet {
public:
	virtual ~degenerate_alphabet() {}
	virtual std::string get_possible_chars(const char c) const = 0;
};

#endif
