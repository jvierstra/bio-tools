#ifndef _DPMATRIX_HH_
#define _DPMATRIX_HH_

#include "matrix.hh"

namespace misc {
	
	template<typename T>
	struct dpcell {
		size_t i, j;
		dpcell *trace;
		T value;
		
		bool operator<(const dpcell& other) const { return value < other.value; }
		bool operator>(const dpcell& other) const { return value > other.value; }
		
		template<typename S> friend std::ostream& operator<<(std::ostream& stream, const dpcell<S>& cell) {
			stream << cell.value;
			return stream;
		}
	};
	
	template<typename T>
	class dpmatrix : public basic_matrix<dpcell<T> > {
	public:
		dpmatrix() : basic_matrix<dpcell<T> >() {}
		dpmatrix(size_t m, size_t n, T val = T()) : basic_matrix<dpcell<T> >(m, n) {
			for (size_t k = 0; k < this->size(); ++k) {
				this->data[k].j = k % n;		
				this->data[k].i = (k-this->data[k].j)/n;	
				this->data[k].value = val;
				this->data[k].trace = 0;
			}
		}
		
		void clear() {
			for(int i = 0; i < this->size(); ++i) {
				this->data[i].value = T(0);
			}
		}
		//TODO: Add a fill function
	};
	
}

#endif
