#ifndef _STL_HH_
#define _STL_HH_

#include <ext/hash_map>
#include <string>

namespace std {
	using namespace __gnu_cxx;
}

namespace __gnu_cxx {
	template<>
	struct hash<std::string> {
		size_t operator()(const std::string& s) const {
			return hash<char const*>()(s.c_str());
		}
	};
}


template<typename iterator>
typename iterator::value_type sum(iterator begin, iterator end, 
	typename iterator::value_type init = typename iterator::value_type()) {
	while(begin != end) {
		init += *begin;
		++begin;
	}
	return init;
}

template<typename iterator, typename unary_func>
typename unary_func::result_type sum(iterator begin, iterator end, unary_func func, 
	typename unary_func::result_type init = typename unary_func::result_type()) {
	while(begin != end) {
		init += f(*begin);
		++begin;
	}
	return init;
}

#endif
