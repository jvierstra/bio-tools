#ifndef _MATRIX_HH_
#define _MATRIX_HH_

#include <ostream>
#include <stdexcept>
#include <functional>
#include <algorithm>
#include <vector>

namespace misc {
	
	template<typename T>
	class basic_matrix {
	public:
		
		typedef T value_type;
		typedef T *pointer;
		typedef const T *const_pointer;
		typedef T &reference;
		typedef const T &const_reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		
		typedef T *iterator;
		typedef const T* const_iterator;
		
	protected:
		
		size_type m, n;
		pointer data;
		
	public:
		
		basic_matrix() : m(0), n(0) {
			data = new value_type[1];
		}

		basic_matrix(size_type m, size_type n, value_type val = value_type()) 
		: m(m), n(n) {
			//allocate space
			data = new value_type[m*n];
			//fill array with default value
			fill(val);
		}
		
		virtual ~basic_matrix() {
			delete[] data;
		}
		
		basic_matrix(const basic_matrix& other) { 
			m = other.m;
			n = other.n;
			data = new value_type[m*n];
			std::copy(other.begin(), other.end(), begin());
		}
		
		virtual basic_matrix& operator=(const basic_matrix& other) {
			if(this != &other) {
				delete[] data;
				m = other.m;
				n = other.n;
				data = new value_type[m*n];
				std::copy(other.begin(), other.end(), begin());  
			}
			return *this;
		}
		
		virtual void resize(size_type nm, size_type nn) {
			if(nm == m and nn == n) {
				return;
			}
		
			pointer ndata = new value_type[nm * nn];
			std::fill(&ndata[0], &ndata[nm*nn], T());
			
			for (size_type i = 0; i < m; ++i) {
				if (i >= nm) break;
				for (size_type j = 0; j < n; ++j) {
					if (j >= nn) break;
					ndata[i*n + j] = data[i*n + j];
				}
			}

			m = nm;
			n = nn;
			delete[] data;
			data = ndata;
		}

		virtual basic_matrix& transpose() {
			pointer ndata = new value_type[m*n];
			for(size_type i = 0; i < m; ++i) {
				for(size_type j = 0; j < n; ++j) {
					ndata[j*m + i] = data[i*n + j];
				}
			}
			
			std::swap(m, n);
			delete[] data;
			data = ndata;
			return *this;
		}
		

		virtual reference operator()(size_type i, size_type j) { 
			if(i < 0  || i >= m || j < 0|| j >= n) 
				throw std::out_of_range("matrix: out-of-bounds!");
			return data[i*n + j]; 
		}
		
		virtual const_reference operator()(size_type i, size_type j) const {
			if(i < 0  || i >= m || j < 0|| j >= n) 
				throw std::out_of_range("matrix: out-of-bounds!");
			return data[i*n + j]; 
		}
		
		/*
		virtual std::vector<value_type> row(size_type i) const { 
			return std::vector<value_type>(n); 
		}
		
		virtual std::vector<value_type> col(size_type j) const {
			std::vector<value_type> res(m);
			for(size_type i = 0; i < m; ++i) {
				res[i] = data[i*n +j];
			}
			return res;
		}
		*/

		virtual void fill(const_reference val) { std::fill(begin(), end(), val); }
		virtual void clear() { fill(value_type()); }
		virtual size_type size() const { return m*n; } 
		virtual size_type rows() const { return m; }
		virtual size_type cols() const { return n; }
		
		virtual iterator begin() { return &data[0]; }
		virtual iterator end() { return &data[size()]; }
		
		virtual const_iterator begin() const { return &data[0]; }
		virtual const_iterator end() const { return &data[size()]; }
		
		template<typename S>
		friend std::ostream& operator<<(std::ostream&, const basic_matrix<S>&);
		
		template<typename S>
		friend std::istream& operator>>(std::istream&, basic_matrix<S>&);
	};
	
	template<typename S>
	inline std::istream& operator>>(std::istream& stream, basic_matrix<S>& mat) {
		bool inited = false;
		typename basic_matrix<S>::size_type rows = 0;
		
		std::string buffer;
		std::vector<S> fields;
		io::line::istream line(stream);
		
		while (line >> buffer) {
			fields.clear();
			misc::string::split(buffer, std::back_inserter(fields));
			
			if (!inited) {	
				if(mat.n != fields.size() or mat.m < 1) {
					mat.resize(10, fields.size());
				}
				inited = true;
			}
		
			if (fields.size() != mat.n) {
				throw std::runtime_error("matrix: input matrix has inconsistent number of columns!");
			}
			
			if (rows == mat.m - 1) {
				mat.resize(int(mat.m * 1.25), mat.n);
			}
			
			for (size_t j = 0; j < mat.n; ++j) {
				mat(rows, j) = boost::lexical_cast<S>(fields[j]);
			}
			
			rows++;
		}
	
		mat.resize(rows, mat.n);
		return stream;
	}
	
	template<typename S>
	inline std::ostream& operator<<(std::ostream& stream, const basic_matrix<S>& mat) {
		for(typename basic_matrix<S>::size_type i = 0; i < mat.m; ++i) {
			for(typename basic_matrix<S>::size_type j = 0; j < mat.n; ++j) {
				stream << mat.data[i*mat.n + j] << " ";
			}
			stream << std::endl;		
		}
		return stream;
	}
}

#endif

