#ifndef _GZ_IFSTREAM_HH_
#define _GZ_IFSTREAM_HH_

#include "fstream.hh"

namespace io { namespace gz {

	class ifstream : public fstream, public std::istream {
	public:
		ifstream() 
			: std::istream(&buf) {}
		explicit ifstream(const std::string& path, int open_mode = std::ios::in)
			: std::istream(&buf) {
			open(path, open_mode);
		}
		filebuf* rdbuf() { return fstream::rdbuf(); }
		void open(const std::string& path, int open_mode = std::ios::in) {
			fstream::open(path, open_mode);
			if(not *this) {
				throw std::runtime_error("Error: Could not open input file: " + path);
			}
		}
	};

} }
#endif
