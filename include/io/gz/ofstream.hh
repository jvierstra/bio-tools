#ifndef _GZ_OFSTREAM_HH_
#define _GZ_OFSTREAM_HH_

#include "fstream.hh"

namespace io { namespace gz {

	class ofstream : public fstream, public std::ostream {
	public:
		ofstream() 
			: std::ostream(&buf) {}
		ofstream(const std::string& path, int open_mode = std::ios::out)
			: std::ostream(&buf) {
			open(path, open_mode);
		}
		filebuf* rdbuf() { return fstream::rdbuf(); }
		void open(const std::string& path, int open_mode = std::ios::out) {
			fstream::open(path, open_mode);
			if(not *this) {
				throw std::runtime_error("Error: Could not open output file: " + path);
			}
		}
	};

} }
#endif
