#ifndef _TABDELIM_OSTREAM_HH_
#define _TABDELIM_OSTREAM_HH_

#include <iterator>
#include <iosfwd>
#include <algorithm>

namespace io { namespace tabdelim {

	class ostream {
	public:
		ostream(std::ostream& stream) : stream(stream) {}
		
		template<typename container>
		ostream& operator<<(const container& fields) {
			std::copy(fields.begin(), fields.end(), std::ostream_iterator<std::string>(stream, "\t"));
			stream << std::endl;			
			return *this;
		}
	protected:
		std::ostream& stream;
	};

} }

#endif
