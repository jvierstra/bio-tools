#ifndef _TABDELIM_ISTREAM_HH_
#define _TABDELIM_ISTREAM_HH_

#include <iosfwd>
#include <iterator>

#include "io/line/istream.hh"
#include "misc/string.hh"

using namespace misc::string;

namespace io { namespace tabdelim {
	
	class istream {
	public:	
		istream(std::istream& stream, const size_t buffer_size = 4 * 1024)
			: stream(stream, buffer_size) {
		}

		operator bool() const { return stream; }
		bool operator!() const { return !stream; }

		template<typename container>
		istream& operator>>(container& fields) {
			if(stream >> line) {

				fields.clear();
				split(line, std::back_inserter(fields), "\t");
			
			}
			return *this;
		}
	
	protected:
		io::line::istream stream;
		std::string line;
	};

} }

#endif
