#ifndef _LINE_ISTREAM_HH_
#define _LINE_ISTREAM_HH_

#include <iosfwd>
#include <algorithm>
#include <vector>

namespace io { namespace line {
	
	class istream {
	public:
		istream(std::istream& stream, const size_t buffer_size = 4*1024)
		: stream(stream), buffer(buffer_size), pos(buffer.end()), end(false) {
			fill_buffer();
			pos = buffer.begin();
			if(pos == buffer.end()) {
				end = true;
			}
		}
		
		istream& operator>>(std::string& line) {
			if(pos == buffer.end()) {
				end = true;
				return *this;
			}
			line.clear();
			std::vector<char>::iterator start = pos;
			pos = std::find(start, buffer.end(), '\n');
			
			while(stream and pos == buffer.end()) {
				line.append(start, pos);
				fill_buffer();
				start = buffer.begin();
				pos = std::find(start, buffer.end(), '\n');
			}			
			line.append(start, pos);
			
			if(pos != buffer.end()) {
				++pos;
				if(stream and pos == buffer.end()) {
					fill_buffer();
					pos = buffer.begin();
				}
			}
			
			return *this;
		}
		
		operator bool() const { return !end; }
		bool operator!() const { return end; }		
		
	protected:
		void fill_buffer() {
			stream.read(&buffer[0], buffer.size());
			if(static_cast<size_t>(stream.gcount()) < buffer.size()) {
				buffer.resize(stream.gcount());
			}
		}
		
		std::istream& stream;
		std::vector<char> buffer;
		std::vector<char>::iterator pos;
		bool end;
	};
	
} }

#endif
